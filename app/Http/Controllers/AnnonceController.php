<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Annonces/Index', [
            'annonces' => Annonce::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
//        $categories = Categorie::all()->pluck('id','libelle');
        $categories = Categorie::all();
        return Inertia::render('Annonces/Create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'titre' => 'required|string',
            'datePublication' => 'required|date',
            'categorie_id' => 'required|exists:categories,id',
            'prix' => 'required',
            'description' => 'required|string|max:200',
        ]);
//        dd($fields);
        Annonce::create($fields);
        return to_route('annonces.create')->with('message', 'l\'annonce a ete bien ajouter.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Annonce $annonce)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Annonce $annonce)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Annonce $annonce)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Annonce $annonce)
    {
        //
    }
}
