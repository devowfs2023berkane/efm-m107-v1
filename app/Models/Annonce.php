<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Annonce extends Model
{
    use HasFactory;

    protected $fillable = [
        'categorie_id',
        'titre',
        'description',
        'datePublication',
        'prix',
    ];

    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }

    public function ventes()
    {
        return $this->hasMany(Vente::class);
    }
}
