<?php

use App\Http\Controllers\AnnonceController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VenteController;
use App\Models\Annonce;
use App\Models\Vente;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    $nmbreAnnonces = Annonce::pluck('id')->count();

//$chiffreTotal = DB::table('annonces')
//    ->join('ventes', 'annonces.id', '=', 'ventes.annonce_id')
//    ->sum('annonces.prix');

    $chiffreTotal = Annonce::join('ventes', 'annonces.id', '=', 'ventes.annonce_id')
        ->sum('annonces.prix');

    return Inertia::render('Dashboard', compact('nmbreAnnonces', 'chiffreTotal'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('ventes', VenteController::class);
    Route::resource('annonces', AnnonceController::class);
});

require __DIR__ . '/auth.php';
